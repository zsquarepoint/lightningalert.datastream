# DTN LightningAlert Datastream

A serverless [AWS Lambda](https://aws.amazon.com/lambda/) function that retrieves lightning and asset data from a predetermined S3 bucket. Generates an alert notification report file and uploads the reports into an AWS S3 bucket. These functions are triggered to run on schedule by AWS Eventbridge.

## Background
DTN creates, ingests, processes, and distributes a large amount of weather data.
A lot of people rely on this data to make critical life and business decisions. As a result, we need to process this data quickly and reliably.
It's imperative that our programs function correctly, self-correct when they can, and leave useful clues about what went wrong and how to fix it when they cannot recover.

## Application Overview
The process starts when a set trigger - S3 Event trigger - occurs:
1. A separate lambda function or external application drops the data sources - lightning.json, assets.json into the target S3 bucket #DataSource folder.
2. On S3 Event trigger, the function will be called.
3. Connects to the S3 bucket data source and retrieves the data sources.
4. Processes the data sources and generates the alert notification file.
5. If alert notifications found, upload the alert notification file into the pre-determined #AlertNotifications folder.

## Getting Started
1. [Software requirements](docs/getting-started.md?anchor=software-requirements)
2. [Solution structure](docs/getting-started.md?anchor=solution-structure)

## Development Setup
1. [Local environment](docs/dev-setup.md?anchor=local-environment)
2. [Configuration files](docs/dev-setup.md?anchor=configuration-files)
3. [Deployment](docs/dev-setup.md?anchor=deployment)
4. [Local testing](docs/dev-setup.md?anchor=local-testing)