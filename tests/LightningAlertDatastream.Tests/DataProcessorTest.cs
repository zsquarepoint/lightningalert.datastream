﻿using Amazon.Lambda.TestUtilities;
using LightningAlertDatastream.Application.Common.Configs;
using LightningAlertDatastream.Functions;
using LightningAlertDatastream.Infrastructure.Configs;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LightningAlertDatastream.Tests
{
    [TestClass]
    public class DataProcessorTest : TestBase
    {
        #region Private Fields

        private DataProcessorFunction _sut;
        private readonly IServiceCollection _serviceCollection;
        private DataConfigs _dataConfigs;

        #endregion Private Fields

        #region Constructor

        public DataProcessorTest()
        {
            _serviceCollection = Startup.BuildContainer();

            //set AWS configs
            var awsConfigs = new AwsConfigs
            {
                SecretKey = "<ENTER YOUR SECRET KEY HERE>",
                AccessKey = "<ENTER YOUR ACCESS KEY HERE>",
                Region = "us-east-1"
            };
            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(AwsConfigs),
                _ => awsConfigs, ServiceLifetime.Singleton));

            //set data configs
            _dataConfigs = new DataConfigs
            {
                BucketName = "aws-lightningalert-bucket",
                DataSourceFolderName = "#DataSource",
                AlertNotificationsFolderName = "#AlertNotifications",
                EmptyLightningData = false
            };
            _serviceCollection.Replace(
                new ServiceDescriptor(typeof(DataConfigs),
                _ => _dataConfigs, ServiceLifetime.Singleton));
        }

        #endregion Constructor

        [TestInitialize]
        public void Init()
        {
        }

        [TestMethod]
        public async Task Run_DataProcessor_Test()
        {
            _sut = new DataProcessorFunction(_serviceCollection.BuildServiceProvider());

            // Act

            //set S3 event notification record
            var evtRec = new Amazon.S3.Util.S3EventNotification.S3EventNotificationRecord
            {
                EventName = RandomGenerator.Generate<string>(),
                EventTime = System.DateTime.Now,
                AwsRegion = "us-east-1",
                EventSource = RandomGenerator.Generate<string>(),
                RequestParameters = new Amazon.S3.Util.S3EventNotification.RequestParametersEntity
                {
                    SourceIPAddress = RandomGenerator.Generate<string>()
                },
                ResponseElements = new Amazon.S3.Util.S3EventNotification.ResponseElementsEntity
                {
                    XAmzRequestId = RandomGenerator.Generate<string>(),
                    XAmzId2 = RandomGenerator.Generate<string>()
                },
                S3 = new Amazon.S3.Util.S3EventNotification.S3Entity
                {
                    ConfigurationId = RandomGenerator.Generate<string>(),
                    Object = new Amazon.S3.Util.S3EventNotification.S3ObjectEntity
                    {
                        ETag = RandomGenerator.Generate<string>(),
                        Key = RandomGenerator.Generate<string>(),
                        Sequencer = RandomGenerator.Generate<string>(),
                        Size = RandomGenerator.Generate<long>(),
                        VersionId = RandomGenerator.Generate<string>()
                    },
                    Bucket = new Amazon.S3.Util.S3EventNotification.S3BucketEntity
                    {
                        Arn = RandomGenerator.Generate<string>(),
                        Name = _dataConfigs.BucketName,
                        OwnerIdentity = new Amazon.S3.Util.S3EventNotification.UserIdentityEntity
                        {
                            PrincipalId = RandomGenerator.Generate<string>()
                        }
                    },
                    S3SchemaVersion = RandomGenerator.Generate<string>(),
                }
            };

            //set S3 event records
            var s3EventRecords = new System.Collections.Generic.List<Amazon.S3.Util.S3EventNotification.S3EventNotificationRecord>();
            s3EventRecords.Add(evtRec);

            //set S3 event model
            var s3Event = new Amazon.Lambda.S3Events.S3Event
            {
                Records = s3EventRecords
            };

            var result = await _sut.Run(s3Event, new TestLambdaContext());

            // Assert
        }

        [TestMethod]
        public async Task Coding_Challenge_Main()
        {
            var x0 = CompareStrings("######a#b#pp##", "a#bpp###");
            var x = CompareStrings("a#b#pp##", "a#bpp###");
            var x1 = CompareStrings("##a#bpp###", "a#b#pp##");

            //var test = Reverse("Ascende Superius");
        }

        public static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        public int CompareStrings(string s1, string s2)
        {
            if (!string.IsNullOrWhiteSpace(s1) && !string.IsNullOrWhiteSpace(s2))
            {
                if (s1 == s2)
                    return 1;

                //trim leading # since they're NOT of no value;
                s1 = s1.TrimStart('#');
                s2 = s2.TrimStart('#');
                if (s1 == s2)
                    return 1;

                //convert to arrays;
                var arrS1 = s1.ToCharArray();
                var arrS2 = s2.ToCharArray();

                var _lS1 = s1.Where(_ => _ != '#');
                var _lS2 = s2.Where(_ => _ != '#');
                if (_lS1 == _lS2)
                    return 1;                

                var _aIdx = new List<int>();
                var tempArr = arrS1;
                for (var x = 0; x < arrS1.Length; x++)
                {
                    if (arrS1[x] != '#')
                        _aIdx.Add(x);

                    if (x == 0)
                    {
                        //arrS1 = Array.sub.r
                    }
                }

                var _bIdx = new List<int>();
                for (var x = 0; x < arrS2.Length; x++)
                {
                    if (arrS1[x] != '#')
                        _bIdx.Add(x);

                    if (x == 0)
                    {
                        //arrS1 = Array.sub.r
                    }
                }

                if (_aIdx == _bIdx)
                    return 1;
            }
            else if (!string.IsNullOrWhiteSpace(s1) && string.IsNullOrWhiteSpace(s2))
            {
                var arrStr = s1.ToCharArray().Where(_ => _ != '#');

                if (arrStr.Any())
                    return 0;
            }
            else if (string.IsNullOrWhiteSpace(s1) && !string.IsNullOrWhiteSpace(s2))
            {
                var arrStr = s2.ToCharArray().Where(_ => _ != '#');

                if (arrStr.Any())
                    return 0;
            }

            return 1;
        }
        
    }


}