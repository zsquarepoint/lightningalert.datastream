﻿using LightningAlertDatastream.Application.Common.Interfaces;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LightningAlertDatastream.Application.Data.Commands
{
    public class ProcessDataSourcesCommandHandler : IRequestHandler<ProcessDataSourcesCommand, string>
    {
        private readonly IDataService _dataService;

        public ProcessDataSourcesCommandHandler(IDataService dataService)
        {
            _dataService = dataService;
        }

        public async Task<string> Handle(ProcessDataSourcesCommand dataSources, CancellationToken cancellationToken)
        {
            return await _dataService.ProcessDataSources(dataSources);
        }
    }
}