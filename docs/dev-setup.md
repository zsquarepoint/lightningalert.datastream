# Development Setup
## Local environment
* Create your own branch based on the **"main"** branch of this repo.
* Clone your newly created branch into your local machine.
* Make sure that the solution builds successfully.
* Run all the unit tests in `tests/LightningAlertDatastream.Tests` and ensure all unit tests passed.

## Configuration files
The application has one configuration `config-pipeline.json`. Describe below are the configuration values that you need to provide or modify:

#### Report configuration
Settings for report file generation.

|Name|Description|
|:---|:---|
|`REPORT_CONFIGS_BUCKET_NAME`|Specifies the AWS S3 bucket where the report files will be uploaded.|
|`DATA_CONFIGS_DATA_SOURCE_FOLDER_NAME`|Specifies the folder name where the data sources are to be retrieved|
|`DATA_CONFIGS_ALERT_NOTIFICATIONS_FOLDER_NAME`|Specifies the folder name where the alert notifications file will be saved.|
|`DATA_CONFIGS_EMPTY_LIGHTNING_DATA`|Specifies the flag whether to empty the lightning data after every data retrieval.|

#### AWS credentials
Contains your own AWS security credentials that will be used when deploying this Lambda function.

|Name|Description|
|:---|:---|
|`AWS_ACCESS_KEY`|Your AWS access key ID|
|`AWS_SECRET_KEY`|Your AWS secret key|
|`AWS_REGION`|The AWS region where this lambda function will be deployed.|

## Deployment
The application will be deployed to AWS Lambda function as a package ZIP file and uses [Serverless Framework](https://www.serverless.com/framework/docs/providers/aws/guide/installation/) to deploy, which also creates AWS infrastructure resources it requires. The serverless configurations that is used in deploying this application to AWS is in the file called **`serverless.yml`**.

#### Deployment package
Defined in the package artifact inside the **`serverless.yml`** configuration is the path to our AWS Lambda Function package in ZIP format.

We can pack the application using Amazon.Lambda.Tools from [.NET Core Global Tool](https://aws.amazon.com/blogs/developer/net-core-global-tools-for-aws/). Follow the steps below to pack the application:

Once Amazon.Lambda.Tools was installed and while on the root directory of the solution:

```
C:\DTN.LightningAlert.Datastream>
```

Assuming that the solution is in the directory above, type the command below:

```
dotnet lambda package --configuration release --framework netcoreapp3.1 --output-package src/LightningAlertDatastream/bin/release/netcoreapp3.1/LightningAlertDatastream-package.zip

--- You should see the output below:
Lambda project successfully packaged: C:\DTN.LightningAlert.Datastream\src\LightningAlertDatastream\bin\release\netcoreapp3.1\LightningAlertDatastream-package.zip
```

If you want to deploy the application from your local envrironment to AWS, you type `serverless deploy` or `sls deploy`

## Local testing
To test the Lambda function locally, you need to do the following:
1. [Install AWS Serverless Application Model CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)
   + AWS SAM is an extension of CloudFormation that further simplifies the process of building serverless application resources.
2. [Install Docker Engine](https://docs.docker.com/engine/install/)
   + AWS SAM CLI requires [Docker](https://www.docker.com/) containers to simulate the AWS Lambda runtime environment on your local development environment.

The solution includes a SAM configuration template for the lambda function. Using the template requires an existing S3 bucket defined in `serverless.yml`, a valid AWS credentials and a valid DTN LightningAlert settings. Each function can be invoked using the code below:

```
--- To extract data processor function, type the code:
sam local invoke -t sam-local-template.yml DataProcessor

```
--- To run / debug the data extractor function: Refer to `tests\LightningAlertDatastream.Tests\DataProcessorTest.cs`
1. Secure your local AWS Credentials - SecretKey, AccessKey and RegionEndpoint - values
2. Supply the above AWS credentials into the `AwsConfigs` model
3. Set data configurations into the `DataConfigs` model
4. Debug / Run `Run_DataProcessor_Test`
