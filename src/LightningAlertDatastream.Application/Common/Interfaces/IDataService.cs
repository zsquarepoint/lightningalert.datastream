﻿using LightningAlertDatastream.Application.Common.Models;
using LightningAlertDatastream.Application.Data.Commands;
using System.Threading.Tasks;

namespace LightningAlertDatastream.Application.Common.Interfaces
{
    public interface IDataService
    {
        Task<DataSourcesResponseModel> GetDataSources();

        Task<string> ProcessDataSources(ProcessDataSourcesCommand dataSources);
    }
}