﻿using System.Text.Json.Serialization;

namespace LightningAlertDatastream.Application.Common.Models
{
    public class Asset
    {
        [JsonPropertyName("assetName")]
        public string AssetName { get; set; }

        [JsonPropertyName("quadKey")]
        public string QuadKey { get; set; }

        [JsonPropertyName("assetOwner")]
        public string AssetOwner { get; set; }
    }
}