﻿using LightningAlertDatastream.Application.Common.Interfaces;
using LightningAlertDatastream.Application.Common.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace LightningAlertDatastream.Application.Data.Queries
{
    public class GetDataSourcesQueryHandler : IRequestHandler<GetDataSourcesQuery, DataSourcesResponseModel>
    {
        private readonly IDataService _dataService;

        public GetDataSourcesQueryHandler(IDataService dataService)
        {
            _dataService = dataService;
        }

        public async Task<DataSourcesResponseModel> Handle(GetDataSourcesQuery request, CancellationToken cancellationToken)
        {
            return await _dataService.GetDataSources();
        }
    }
}