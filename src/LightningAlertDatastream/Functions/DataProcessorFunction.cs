using Amazon.Lambda.Core;
using Amazon.Lambda.S3Events;
using LightningAlertDatastream.Application.Data.Commands;
using LightningAlertDatastream.Application.Data.Queries;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Threading.Tasks;

namespace LightningAlertDatastream.Functions
{
    public class DataProcessorFunction
    {
        private readonly IServiceProvider _serviceProvider;

        #region Constructor

        public DataProcessorFunction() : this(Startup.BuildContainer().BuildServiceProvider())
        {
        }

        public DataProcessorFunction(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion Constructor

        [LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]
        public async Task<string> Run(S3Event evnt, ILambdaContext context)
        {
            context.Logger.LogLine($"Lightning Alert data processor invoked: {DateTime.Now}");

            var s3Event = evnt.Records?[0].S3;

            if (s3Event == null)
            {
                return "S3 upload notification returns empty record.";
            }

            if (s3Event.Object.Key.EndsWith('/'))
            {
                return "S3 upload notification has no file specified.";
            }

            //init service provider
            var mediator = _serviceProvider.GetService<IMediator>();

            //get schema content
            var dataSources = await mediator.Send(new GetDataSourcesQuery());
            var strikeCount = dataSources.LightningStrikes.Count;
            var assetCount = dataSources.Assets.Count;

            //process data sources alerts
            var alertResponse = "";
            if (strikeCount > 0 && assetCount > 0)
            {
                alertResponse = await mediator.Send(new ProcessDataSourcesCommand
                {
                    Assets = dataSources.Assets,
                    Strikes = dataSources.LightningStrikes
                });
            }

            var runMessage = $"The Lightning-Alert data processor run successfully. Lighning Strikes: {strikeCount}; Assets: {assetCount}";
            if (!string.IsNullOrWhiteSpace(alertResponse))
            {
                runMessage += $"; Alerts Issued: {alertResponse}";
            }

            Console.WriteLine(runMessage);
            return runMessage;
        }
    }
}