﻿using LightningAlertDatastream.Application.Common.Models;
using MediatR;
using System.Collections.Generic;

namespace LightningAlertDatastream.Application.Data.Commands
{
    public class ProcessDataSourcesCommand : IRequest<string>
    {
        public List<Asset> Assets { get; set; }

        public List<LightningStrike> Strikes { get; set; }
    }
}