﻿using System;
using System.Text.Json.Serialization;

namespace LightningAlertDatastream.Application.Common.Models
{
    public class LightningStrike
    {
        [JsonPropertyName("flashType")]
        public int FlashType { get; set; }

        [JsonPropertyName("strikeTime")]
        public Int64 StrikeTime { get; set; }

        [JsonPropertyName("latitude")]
        public double Latitude { get; set; }

        [JsonPropertyName("longitude")]
        public double Longitude { get; set; }

        [JsonPropertyName("peakAmps")]
        public int PeakAmps { get; set; }

        [JsonPropertyName("reserved")]
        public string Reserved { get; set; }

        [JsonPropertyName("icHeight")]
        public int icHeight { get; set; }

        [JsonPropertyName("receivedTime")]
        public Int64 ReceivedTime { get; set; }

        [JsonPropertyName("numberOfSensors")]
        public int NumberOfSensors { get; set; }

        [JsonPropertyName("multiplicity")]
        public int Multiplicity { get; set; }

        public DateTimeOffset StrikeDateTime
        {
            get
            {
                return DateTimeOffset.FromUnixTimeMilliseconds(StrikeTime);
            }
        }

        public DateTimeOffset ReceivedDateTime
        {
            get
            {
                return DateTimeOffset.FromUnixTimeMilliseconds(ReceivedTime);
            }
        }
    }
}