﻿namespace LightningAlertDatastream.Application.Common.Configs
{
    public class DataConfigs
    {
        public string BucketName { get; set; }

        public string DataSourceFolderName { get; set; } = "#DataSource";

        public string AlertNotificationsFolderName { get; set; } = "#AlertNotifications";

        public bool EmptyLightningData { get; set; } = false;
    }
}