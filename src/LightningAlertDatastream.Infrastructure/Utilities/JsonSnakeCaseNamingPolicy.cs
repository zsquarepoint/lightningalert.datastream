﻿using LightningAlertDatastream.Application.Extensions;
using System.Text.Json;

namespace LightningAlertDatastream.Infrastructure.Utilities
{
    public class JsonSnakeCaseNamingPolicy : JsonNamingPolicy
    {
        public static JsonSnakeCaseNamingPolicy Instance { get; } = new JsonSnakeCaseNamingPolicy();

        public override string ConvertName(string name)
        {
            return name.ToSnakeCase();
        }
    }
}