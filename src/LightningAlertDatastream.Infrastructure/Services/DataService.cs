﻿using Amazon.Extensions.NETCore.Setup;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon.SimpleSystemsManagement;
using LightningAlertDatastream.Application.Common.Configs;
using LightningAlertDatastream.Application.Common.Interfaces;
using LightningAlertDatastream.Application.Common.Models;
using LightningAlertDatastream.Application.Data.Commands;
using LightningAlertDatastream.Infrastructure.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace LightningAlertDatastream.Infrastructure.Services
{
    public class DataService : IDataService
    {
        #region Private Fields

        private readonly IAmazonS3 _s3Client;
        private readonly IAmazonSimpleSystemsManagement _ssmClient;
        private readonly IMappingService _mappingService;
        private readonly DataConfigs _dataConfigs;
        private readonly string _bucketName;
        private readonly string _dataSourceFolderName;
        private readonly string _alertNotificationsFolderName;
        private readonly JsonSerializerOptions _jsonSerializerOptions;

        #endregion Private Fields

        #region Constructor

        public DataService(IMappingService mappingService, AWSOptions awsOptions, DataConfigs dataConfigs)
        {
            _mappingService = mappingService;
            _s3Client = awsOptions.CreateServiceClient<IAmazonS3>();
            _ssmClient = awsOptions.CreateServiceClient<IAmazonSimpleSystemsManagement>();
            _dataConfigs = dataConfigs;

            _bucketName = _dataConfigs.BucketName;
            _dataSourceFolderName = _dataConfigs.DataSourceFolderName;
            _alertNotificationsFolderName = _dataConfigs.AlertNotificationsFolderName;

            _jsonSerializerOptions = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonSnakeCaseNamingPolicy.Instance
            };
            _jsonSerializerOptions.Converters.Add(new ObjectToInferredTypesConverter());
        }

        #endregion Constructor

        #region Public Methods

        /// <summary>
        /// This method retrieves the data sources coming a set S3 bucket #DataSource
        /// </summary>
        /// <returns>Data sources response model containing the list of lightning strikes and assets</returns>
        public async Task<DataSourcesResponseModel> GetDataSources()
        {
            var assetList = new List<Asset>();
            var strikeList = new List<LightningStrike>();

            try
            {
                //set bucket request info
                var request = new ListObjectsV2Request
                {
                    BucketName = _bucketName,
                    Prefix = _dataSourceFolderName
                };

                ListObjectsV2Response response;

                do
                {
                    response = await _s3Client.ListObjectsV2Async(request);

                    //loop each S3 objects; read objects inside the #DataSource folder
                    foreach (S3Object entry in response.S3Objects)
                    {
                        if (entry.Key.Contains(_dataSourceFolderName) && entry.Key != $"{_dataSourceFolderName}/")
                        {
                            var objResponse = await _s3Client.GetObjectAsync(new GetObjectRequest
                            {
                                BucketName = _bucketName,
                                Key = entry.Key,
                            });

                            var dataContent = new StreamReader(objResponse?.ResponseStream)?.ReadToEnd();

                            if (!string.IsNullOrEmpty(dataContent))
                            {
                                if (entry.Key.Contains("asset"))
                                {
                                    assetList = ProcessAssetData(assetList, dataContent);
                                }
                                else if (entry.Key.Contains("lightning"))
                                {
                                    strikeList = ProcessStrikeData(strikeList, dataContent);

                                    //empty out the lightning.json file
                                    if (_dataConfigs.EmptyLightningData)
                                    {
                                        await SetEmptyData(entry.Key);
                                    }
                                }
                            }
                        }
                    }

                    request.ContinuationToken = response.NextContinuationToken;
                } while (response.IsTruncated);
            }
            catch (AmazonS3Exception s3Ex)
            {
                Console.WriteLine("S3 error occurred. Exception: " + s3Ex.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error retrieving data source files from S3 bucket. Error: {0}", ex.Message);
            }

            //return list
            return new DataSourcesResponseModel
            {
                Assets = assetList,
                LightningStrikes = strikeList
            };
        }

        /// <summary>
        /// This method processes the data sources - lightning strikes and assets
        /// </summary>
        /// <param name="dataSources"></param>
        /// <returns>A concatenated string of lightning alert notifications separated by a comma</returns>
        public async Task<string> ProcessDataSources(ProcessDataSourcesCommand dataSources)
        {
            var alertNotifications = new List<string>();

            //loop through lightning strikes
            foreach (var strike in dataSources.Strikes)
            {
                //this mapping service is just a sample taken from Microsoft documentation
                //link: https://docs.microsoft.com/en-us/bingmaps/articles/bing-maps-tile-system
                //this needs to be refined in terms of 'Level Of Detail'
                var quadKey = await _mappingService.GetQuadKey(strike);

                if (!string.IsNullOrWhiteSpace(quadKey))
                {
                    var asset = dataSources.Assets.Find(_ => _.QuadKey == quadKey);

                    if (asset != null)
                    {
                        //set alert text
                        var alertText = $"lightning alert for {asset.AssetOwner}:{asset.AssetName}";

                        //check if alert already exists; no double alerts allowed;
                        if (alertNotifications.IndexOf(alertText) < 0)
                        {
                            alertNotifications.Add(alertText);
                        }
                    }
                }
            }

            //push alert notifications to bucket
            if (alertNotifications.Count > 0)
            {
                await UploadAlertNotifications(alertNotifications);
            }

            return string.Join(",", alertNotifications);
        }

        #endregion Public Methods

        #region Private Methods

        private List<Asset> ProcessAssetData(List<Asset> assetList, string rawData)
        {
            if (!string.IsNullOrWhiteSpace(rawData))
            {
                try
                {
                    //deserialize to assets
                    var assets = JsonSerializer.Deserialize<List<Asset>>(rawData, _jsonSerializerOptions);

                    if (assets.Count > 0)
                    {
                        assetList.AddRange(assets.Distinct());
                    }
                }
                catch
                {
                    //do nothing; it maybe there's an unknown file included;
                }
                
            }

            return assetList;
        }

        private Dictionary<string,string> ProcessAssetDataToDictionary(string rawData)
        {
            if (!string.IsNullOrWhiteSpace(rawData))
            {
                try
                {
                    //deserialize to assets
                    var assets = JsonSerializer.Deserialize<List<Asset>>(rawData, _jsonSerializerOptions);

                    if (assets.Count > 0)
                    {
                        var uniqueAssets = (from a in assets group a by a.QuadKey into g select g.First()).ToList();

                        return uniqueAssets.Distinct().ToDictionary(_ => _.QuadKey, _ => $"{_.AssetOwner}:{_.AssetName}");
                    }
                }
                catch
                {
                    //do nothing; it maybe there's an unknown file included;
                }

            }

            return new Dictionary<string, string>();
        }

        private List<LightningStrike> ProcessStrikeData(List<LightningStrike> strikeList, string rawData)
        {
            if (!string.IsNullOrWhiteSpace(rawData))
            {
                var arrStrikes = rawData.Split("\n"); //raw-data comes in text lines

                if (arrStrikes.Length > 0)
                {
                    //loop through all strikes
                    foreach (var strike in arrStrikes)
                    {
                        try
                        {
                            if (string.IsNullOrWhiteSpace(strike))
                                continue;

                            //deserialize to lightning-strike
                            var strikeData = JsonSerializer.Deserialize<LightningStrike>(strike, _jsonSerializerOptions);

                            //add to list
                            if (strikeData != null && (strikeData.FlashType == 0 || strikeData.FlashType == 1))
                            {
                                strikeList.Add(strikeData);
                            }
                        }
                        catch(Exception ex)
                        {
                            Console.WriteLine("Error deserializing strike data. Data: {0} Error: {1}", strike.ToString(), ex.Message);
                        }
                    }
                }
            }

            return strikeList;
        }

        private async Task SetEmptyData(string keyName)
        {
            try
            {
                //convert data to memory stream
                using var dataStream = new MemoryStream();
                byte[] memstring = new UTF8Encoding(true).GetBytes(string.Empty);
                dataStream.Write(memstring, 0, memstring.Length);

                //upload file to S3 bucket
                await UploadFile(dataStream, keyName);
            }
            catch (Exception ex)
            {
                throw new Exception($"Error emptying data. Error: {ex.Message}");
            }
        }

        private async Task UploadAlertNotifications(List<string> alertNotifications)
        {
            try
            {
                string jsonAlertData = JsonSerializer.Serialize(alertNotifications);

                //convert data to memory stream
                using var dataStream = new MemoryStream();
                byte[] memstring = new UTF8Encoding(true).GetBytes(jsonAlertData);
                dataStream.Write(memstring, 0, memstring.Length);

                //construct key name
                var keyName = $"{_alertNotificationsFolderName}/LightningAlerts{DateTime.Now:yyyy-MM-dd-HHmm}.json";

                //upload file to S3 bucket
                await UploadFile(dataStream, keyName);
            }
            catch (Exception ex)
            {
                throw new Exception($"Error uploading alert notifications. Error: {ex.Message}");
            }
        }

        private async Task UploadFile(MemoryStream dataStream, string keyName)
        {
            try
            {
                var transferUtility = new TransferUtility(_s3Client);
                await transferUtility.UploadAsync(dataStream, _bucketName, keyName);
            }
            catch (Exception ex)
            {
                throw new Exception($"Error uploading stream file ({keyName}) to S3 bucket. Error: {ex.Message}");
            }
        }

        #endregion Private Methods
    }
}