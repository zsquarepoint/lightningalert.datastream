using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Spackle;
using System.Collections.Generic;

namespace LightningAlertDatastream.Tests
{
    public abstract class TestBase
    {
        protected RandomObjectGenerator RandomGenerator { get; }
        private readonly List<Mock> _mocks;

        protected TestBase()
        {
            RandomGenerator = new RandomObjectGenerator();
            _mocks = new List<Mock>();
        }

        protected Mock<T> CreateMock<T>() where T : class
        {
            var mockClass = new Mock<T>(MockBehavior.Strict);

            //add to _mocks list
            _mocks.Add(mockClass);

            return mockClass;
        }

        [TestCleanup]
        public void BaseCleanup()
        {
            foreach (var mock in _mocks)
            {
                mock.VerifyAll();
            }
        }
    }
}