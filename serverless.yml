service: dtn-lightningalert-datastream

frameworkVersion: '2.66.1'
variablesResolutionMode: 20210326
useDotenv: true

provider:
  name: aws
  runtime: dotnetcore3.1
  region: eu-west-1
  environment:
    # Data Config Settings    
    DataConfigs__BucketName: ${file(./config/config.${self:custom.stage}.json):DATA_CONFIGS_BUCKET_NAME, ""}  
    DataConfigs__DataSourceFolderName: ${file(./config/config.${self:custom.stage}.json):DATA_CONFIGS_DATA_SOURCE_FOLDER_NAME, "#DataSource"}
    DataConfigs__AlertNotificationsFolderName: ${file(./config/config.${self:custom.stage}.json):DATA_CONFIGS_ALERT_NOTIFICATIONS_FOLDER_NAME, "#AlertNotifications"}
    DataConfigs__EmptyLightningData: ${file(./config/config.${self:custom.stage}.json):DATA_CONFIGS_EMPTY_LIGHTNING_DATA, false}

    # AWS Configuration
    AwsConfigs__AccessKey: ${file(./config/config.${self:custom.stage}.json):AWS_ACCESS_KEY, ""}
    AwsConfigs__SecretKey: ${file(./config/config.${self:custom.stage}.json):AWS_SECRET_KEY, ""}
    AwsConfigs__Region: ${file(./config/config.${self:custom.stage}.json):AWS_REGION, ""}            

  lambdaHashingVersion: 20201221  
  memorySize: 4096
  timeout: 900
  logRetentionInDays: 1
  stage: ${self:custom.stage}
  deploymentBucket:
    name: ${self:provider.environment.DataConfigs__BucketName}-deployment
    serverSideEncryption: AES256
    blockPublicAccess: true
    tags:
      Module: ${self:custom.tags.module}
      Owner: ${self:custom.tags.owner}
      Project: ${self:custom.tags.project}
      Role: ${self:custom.tags.role}
      Service: ${self:service}
      Environment: ${self:custom.stage}
  tags:
    Module: ${self:custom.tags.module}
    Owner: ${self:custom.tags.owner}
    Project: ${self:custom.tags.project}
    Role: ${self:custom.tags.role}
    Service: ${self:service}
    Environment: ${self:custom.stage}
  stackTags:
    Module: ${self:custom.tags.module}
    Owner: ${self:custom.tags.owner}
    Project: ${self:custom.tags.project}
    Role: ${self:custom.tags.role}
    Service: ${self:service}
    Environment: ${self:custom.stage}

  iam:
    role:
      statements:
        - Effect: 'Allow'
          Action:
            - s3:ListBucket
            - s3:GetObject
            - s3:GetObjectAcl
            - s3:PutObject
            - s3:PutObjectAcl
          Resource:
            - 'arn:aws:s3:::${self:provider.environment.DataConfigs__BucketName}'
            - 'arn:aws:s3:::${self:provider.environment.DataConfigs__BucketName}/*'              

functions:
  DataProcessor:
    handler: LightningAlertDatastream::LightningAlertDatastream.Functions.DataProcessorFunction::Run
    name: ${self:service}-function
    events:
      - s3:
          bucket: ${self:provider.environment.DataConfigs__BucketName}
          event: s3:ObjectCreated:*
          rules:
            - prefix: ${self:provider.environment.DataConfigs__DataSourceFolderName}/
          existing: true

plugins:
  - serverless-deployment-bucket

custom:
  defaultStage: pipeline
  stage: ${opt:stage, self:custom.defaultStage}
  tags:
    owner: Jaite Retuya
    project: DTN Lightning Alert Datastream
    role: LightningAlertDataStream
    module: DTN
  deploymentBucket:
    blockPublicAccess: true
    tags:
      - Key: Owner
        Value: ${self:custom.tags.owner}
      - Key: Project
        Value: ${self:custom.tags.project}
      - Key: Role
        Value: ${self:custom.tags.role}
      - Key: Module
        Value: ${self:custom.tags.module}
      - Key: Environment
        Value: ${self:custom.stage}
      - Key: Service
        Value: ${self:service}

resources:
  Resources:
    S3DTNLightningAlertBucket:
      Type: 'AWS::S3::Bucket'
      Properties:
        BucketName: ${self:provider.environment.DataConfigs__BucketName}
        BucketEncryption:
          ServerSideEncryptionConfiguration:
            - ServerSideEncryptionByDefault:
                SSEAlgorithm: AES256
        PublicAccessBlockConfiguration:
          BlockPublicAcls: true
          BlockPublicPolicy: true
          IgnorePublicAcls: true
          RestrictPublicBuckets: true