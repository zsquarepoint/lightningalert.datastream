﻿using LightningAlertDatastream.Application.Common.Models;
using MediatR;

namespace LightningAlertDatastream.Application.Data.Queries
{
    public class GetDataSourcesQuery : IRequest<DataSourcesResponseModel>
    {
    }
}