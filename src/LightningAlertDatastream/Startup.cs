﻿using LightningAlertDatastream.Application;
using LightningAlertDatastream.Application.Common.Configs;
using LightningAlertDatastream.Extensions;
using LightningAlertDatastream.Infrastructure;
using LightningAlertDatastream.Infrastructure.Configs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace LightningAlertDatastream
{
    public class Startup
    {
        public static IServiceCollection BuildContainer()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            return ConfigureServices(configuration);
        }

        private static IServiceCollection ConfigureServices(IConfigurationRoot configurationRoot)
        {
            var services = new ServiceCollection();

            services
                .AddDefaultAWSOptions(configurationRoot.GetAWSOptions())
                .AddApplication()
                .AddInfrastructure()
                .BindAndConfigure(configurationRoot.GetSection(nameof(DataConfigs)), new DataConfigs())
                .BindAndConfigure(configurationRoot.GetSection(nameof(AwsConfigs)), new AwsConfigs());

            return services;
        }
    }
}