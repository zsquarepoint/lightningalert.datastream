﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace LightningAlertDatastream.Application
{
    public static class ServiceRegistration
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services
                .AddMediatR(Assembly.GetExecutingAssembly());

            return services;
        }
    }
}