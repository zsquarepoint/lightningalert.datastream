﻿using System.Collections.Generic;

namespace LightningAlertDatastream.Application.Common.Models
{
    public class DataSourcesResponseModel
    {
        public List<Asset> Assets { get; set; } = new List<Asset>();

        public List<LightningStrike> LightningStrikes { get; set; } = new List<LightningStrike>();
    }
}